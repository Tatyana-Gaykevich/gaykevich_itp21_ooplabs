﻿using System;
using CircleLib;

namespace Application
{
    class Program
    {
        static void Main(string[] args)
        {
            int circleCenterX = 0, circleCenterY = 0 , circleRadius = 0;

            bool flag = false;
            while (flag == false)
            {
                Console.Write("Enter value X: ");
                circleCenterX = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter value Y: ");
                circleCenterY = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter value R: ");
                circleRadius = Convert.ToInt32(Console.ReadLine());

                flag = Circle.IsExist(circleRadius);
                if (flag == true)
                    Console.WriteLine("Figure Exist");
                else
                    Console.WriteLine("Figure does not Exist");
            }


            Circle figure1 = new Circle(circleCenterX, circleCenterY, circleRadius);
            //Circle figure1 = new Circle(3, 2, 3);
            figure1.ShowInfo();

            Console.Write("Check if the point of the circle belongs? 1 - yes, 2 - no\n");
            int point1 = Convert.ToInt32(Console.ReadLine());
            if (point1 == 1)
            {
                int x, y;
                Console.WriteLine("Entet X");
                x = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Entet Y");

                y = Convert.ToInt32(Console.ReadLine());
                bool result = figure1.IsPoint(x, y);
                Console.Write(result);
            }
            else
                Console.Write("No so no\n");

            Console.ReadKey();
        }
    }
}
