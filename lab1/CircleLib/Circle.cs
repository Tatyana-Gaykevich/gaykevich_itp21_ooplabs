﻿using System;

namespace CircleLib
{
    public class Circle
    {
        private int centerX; // X coordinate
        private int centerY; // Y coordinate
        private int radius;  //Сircle radius
       // public int x;
       // public int y;
       // public int point;


        /// <summary>
        /// Сonstructor
        /// </summary>
        /// <param name="circleCenterX"></param>
        /// <param name="circleCenterY"></param>
        /// <param name="circleRadius"></param>
        public Circle(int circleCenterX, int circleCenterY, int circleRadius)
        {

            centerX = circleCenterX;
            centerY = circleCenterY;
            radius = circleRadius;
        }

       
        /// <summary>
        /// Method for calculating the perimeter of a circle
        /// </summary>
        /// <returns>Circle perimeter</returns>
        //Метод для вычисления периметра окружности
        public string Perimeter()
        {
            return (2 * Math.PI * radius).ToString();
        }

        /// <summary>
        /// Method for calculating the area of a circle
        /// </summary>
        /// <returns>Circle area</returns>
        public string Area()
        {
            return (Math.PI * Math.Pow(radius, 2)).ToString();
        }

        /// <summary>
        /// This is a method that checks whether a circle point belongs
        /// </summary>
        public bool IsPoint(int x, int y)
        {

            if (Math.Pow(centerX - x, 2) + Math.Pow(centerY - y, 2) <= Math.Pow(radius, 2))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Method for checking the existence of this figure
        /// </summary>
        public static bool IsExist(int radius)
        {
            if (radius < 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Method for displaying information
        /// </summary>
        public void ShowInfo()
        {
            Console.WriteLine(String.Format("Square is equal to {0:f2}\n" +
                "Perimeter is equal to {1:f2}\n", Convert.ToString(Area()), Convert.ToString(Perimeter())));
        }
    }
}
