﻿using System;

namespace MatrixLib
{
    public class Matrix
    {

        double[,] matrix;
        int matrixRow, matrixColumn;

        //Матрица row x column
        public Matrix(int row, int column)
        {
            matrix = new double[row, column];
            matrixRow = row;
            matrixColumn = column;
        }

            //Перегрузка индексатора, чтобы обратится к 
            //элементу матрицы как к элементу двумерного массива
        public double this[int i, int j]
        {
            get { return matrix[i, j]; }
            set { matrix[i, j] = value; }
        }

        //Умножение на число
        public static Matrix operator *(Matrix m, int value)
        {
            Matrix mat = new Matrix(m.matrixRow, m.matrixColumn);
            for (int i = 0; i < m.matrixRow; i++)
                for (int j = 0; j < m.matrixColumn; j++)
                    mat[i, j] = m[i, j] * value;
            return mat;
        }

        public static Matrix operator *(int value, Matrix m)
        {
            return m*value;
        }

        //Распечатать матрицу
        public void PrintMatrix()
        {
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                    Console.Write("{0}   ", this[i, j]);
                Console.Write("\n");
            }

        }

        //Произведение матриц
        public static Matrix operator *(Matrix first, Matrix second)
        {
            Matrix matr = new Matrix(first.matrixRow, first.matrixColumn);
            for (int i = 0; i < first.matrixRow; i++)
            {
                for (int j = 0; j < second.matrixColumn; j++)
                {
                    double sum = 0;
                    for (int r = 0; r < first.matrixColumn; r++)
                        sum += first[i, r] * second[r, j];
                    matr[i, j] = sum;
                }
            }
            return matr;
        }
        }
}