﻿using System;
using MatrixLib;


namespace MatrixApp
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int row, column;
            Console.WriteLine("Enter the number of matrix rows: ");
            row = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter the number of matrix columns: ");
            column = Convert.ToInt32(Console.ReadLine());

            Random rand = new Random();
            Matrix first = new Matrix(row, column);
            Matrix second = new Matrix(row, column);


            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++)
                {
                    first[i, j] = rand.Next(1, 10);
                    second[i, j] = rand.Next(1, 10);
                }

            Console.WriteLine("First matrix:\n");  //Первая матрица
            first.PrintMatrix();

            Console.WriteLine("Second matrix:\n"); //Вторая матрица
            second.PrintMatrix();

            bool flag = true;
            while (flag == true)
            {
                Console.WriteLine("\n\n\t----- MENU -----\n" +
            "1. Matrix 1 * Matrix 2" +
            "2. Matrix * Number\n" +
            "3. EXIT\n");

                int number = Convert.ToInt32(Console.ReadLine());
                switch (number)
                {
                    case 1:
                        //Console.WriteLine("\n\nВторая матрица:\n\n");
                        (first * second).PrintMatrix();
                        break;
                    case 2:
                        Console.WriteLine("Insert the number: ");
                        int value = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("\nMultiplication by a Number:\n"); //Умножение на число
                        (first * value).PrintMatrix();
                        break;
                    case 3:
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("This input is undefined");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
    

