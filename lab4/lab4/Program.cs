﻿/*using System;
using System.Text.RegularExpressions;
using StringLib;


namespace StringApp
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            bool flag = true;
            while (flag == true)
            {
                Console.WriteLine("\t----- MENU -----\n" +
            "1. TASK 1\n" +
            "2. TASK 2\n" +
            "3. EXIT\n");

                int num = Convert.ToInt32(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Console.WriteLine("Enter a string to analyse"); //Ввод строки для анализа
                        string mystr = Console.ReadLine();
                        Console.WriteLine("The number of symbols is " + StringClass.DiffSymbols(mystr).Length);
                        Console.WriteLine("They are:");
                        Console.Write(StringClass.DiffSymbols(mystr));
                        Console.ReadKey();
                        break;
                    case 2:
                        string path = @"/Users/tatana/Desktop/file.txt";
                        Console.WriteLine("Enter phone number");
                        string number = Console.ReadLine();
                        string pattern = @"^\s*\+\d{12}\s*$";
                        if (Regex.IsMatch(number, pattern, RegexOptions.IgnoreCase))
                        // IsMatch - проверка на соответсятвие стоки формату 
                        {
                            Console.WriteLine("Phone number confirmed");
                            Console.WriteLine(StringClass.SpecStr(path, number));
                            Console.ReadKey();
                        }
                        else Console.WriteLine("The number is incorrect");
                        Console.ReadKey();
                        break;
                    case 3:
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("This input is undefined");
                        break;
                }
            }
        }
    }
}
*/

using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
 
internal sealed class Program
{
    static void Main()
    {
        var matchesA = new Regex(@"((0[0-9][0-9]|[1][0-1][0-9]|12[0-7])\.\d{3}\.\d{3}\.\d{3})").Matches(
          File.ReadAllText(@"/Users/tatana/Desktop/file1.txt", Encoding.Default)
        ).Cast<Match>().Select(
          v => v.Value
        );
        Console.WriteLine("A");
        foreach (var m in matchesA) Console.WriteLine(m);

        var matchesB = new Regex(@"((12[8-9]|1[3-8][0-9]|19[0-1])\.\d{3}\.\d{3}\.\d{3})").Matches(
          File.ReadAllText(@"/Users/tatana/Desktop/file1.txt", Encoding.Default)
        ).Cast<Match>().Select(
          v => v.Value
        );
        Console.WriteLine("B");
        foreach (var m in matchesB) Console.WriteLine(m);

        var matchesC = new Regex(@"(([1][9][2-9]|[2][0-1][0-9]|[2][2][0-3])\.\d{3}\.\d{3}\.\d{3})").Matches(
          File.ReadAllText(@"/Users/tatana/Desktop/file1.txt", Encoding.Default)
        ).Cast<Match>().Select(
          v => v.Value
        );
        Console.WriteLine("C");
        foreach (var m in matchesC) Console.WriteLine(m);

        var matchesD = new Regex(@"((22[4-9]|23[0-9])\.\d{3}\.\d{3}\.\d{3})").Matches(
          File.ReadAllText(@"/Users/tatana/Desktop/file1.txt", Encoding.Default)
        ).Cast<Match>().Select(
          v => v.Value
        );
        Console.WriteLine("D");
        foreach (var m in matchesD) Console.WriteLine(m);

        var matchesE = new Regex(@"((24[0-9]|25[0-5])\.\d{3}\.\d{3}\.\d{3})").Matches(
          File.ReadAllText(@"/Users/tatana/Desktop/file1.txt", Encoding.Default)
        ).Cast<Match>().Select(
          v => v.Value
        );
        Console.WriteLine("E");
        foreach (var m in matchesE) Console.WriteLine(m);

    }
}