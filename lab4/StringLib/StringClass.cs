﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;


namespace StringLib
{
    public class StringClass
    {
        public static char[] DiffSymbols(string mystr)
        {
                int len = mystr.Length;
                char[] symb = new char[len];
                int k = 0;
                bool ex = true;
                foreach (char i in mystr)
                {
                    for (int j = 0; j < k; j++)
                    {
                        if (symb[j] == i)
                        {
                            ex = false;
                            break;
                        }
                    }
                    if (ex)
                    {
                        symb[k] = i;
                        k++;
                    }
                    ex = true;
                }
                Array.Resize(ref symb, k); // Resize - изменяет размер одномерного массива 
                return symb;
        }

        public static StringBuilder SpecStr(string path, string number)
        {
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                string line;
                StringBuilder sb = new StringBuilder();
                while ((line = sr.ReadLine()) != null)
                {
                    string[] elements = line.Split(new char[] { ',' });
                    try
                    {
                        string pattern = @"(^(\s*\+\d{12}\s*),(\s*(?:0?[0-9]|1[0-9]|2[0-3])\-[0-5][0-9]\s*),(\s*[0-9]+)$)";

                        if (Regex.IsMatch(line, pattern, RegexOptions.IgnoreCase))
                        // IsMatch - проверка на соответсятвие стоки формату 
                        {
                            if (CheckString(line, number))
                            {
                                sb.Append(line + ";\n");
                            }
                        }
                    }
                    catch
                    {
                        Console.WriteLine(line + "- incorrect data");
                    }

                }
                if (sb.Length == 0)
                {
                    sb.Append("There were no calls to this number");
                }
                return sb;
            }
        }

        public static bool CheckString(string str, string number)
        {
            bool answ = false;
            string[] elements = str.Split(new char[] { ',' });
            string[] timeH = elements[1].Split(new char[] { '-' });
            int time = int.Parse(timeH[0]);
            if (elements[0] == number && time >= 0 && time < 12)
            {
                answ = true;
            }
            return answ;
        }
    }
}
