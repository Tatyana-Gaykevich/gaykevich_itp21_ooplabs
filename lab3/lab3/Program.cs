﻿using System;
using MatrixLib;

namespace MatrixApp
{
    class MainClass
    {
        static void Main(string[] args)
        {
            int row, column;
            Console.WriteLine("Enter the number of matrix rows: ");
            row = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the number of matrix columns: ");
            column = Convert.ToInt32(Console.ReadLine());
            
            Random rand = new Random();

            Matrix first = new Matrix(row, column)
            {
                NameM = "First matrix"
            };
            string firstName = first.NameM;
            
            Matrix second = new Matrix(row, column);
            second.NameM = "Second matrix";
            string secondName = second.NameM;

            Matrix third = new Matrix(row, column);
            third.NameM = "Third matrix";
            string thirdName = third.NameM;

            for (int i = 0; i < row; i++)
                for (int j = 0; j < column; j++)
                {
                    first[i, j] = rand.Next(-10, 10);
                    second[i, j] = rand.Next(1, 10);
                    third[i, j] = rand.Next(1, 10);
                }

            bool flag = true;
            while (flag) 
            {
                Console.WriteLine("\n\n\t----- MENU -----\n" +
            "1. Output of three matrices\n" +
            //Вывод трёх матриц
            "2. Calculation and output of the product of negative array elements\n" +
            // Вычисление и вывод произведения отрицательных элементов массива
            "3. Calculation of the sum of arrays with the same dimensions\n" +
            // Вычисление суммы массивов с одинаковыми размерностями
            "4. If the product of the negative elements of the first array is greater than the given number,\n" +
            "and in the third matrix there are nonzero elements, increase all the negative elements of this array\n" +
            "by the minimum value among the elements of the last row of the third array\n" +
            // Если произведение отрицательных элементов первого массива больше заданного числа,
            //а в третьей матрице есть ненулеве элементы, увеличить все отрицательные элементы этого массива
            //на значение минимального среди элементов последней строки третьего массива
            "5. EXIT\n");

                int number = Convert.ToInt32(Console.ReadLine());
                switch (number)
                {
                    case 1:
                        Console.WriteLine(firstName);  //Первая матрица
                        first.PrintMatrix();
                        Console.WriteLine(secondName); //Вторая матрица
                        second.PrintMatrix();
                        Console.WriteLine(thirdName);  //Третья матрица
                        third.PrintMatrix();
                        break;
                    case 2:
                        first.Multyply(int[,] matrix, out int countOfZero);
                        break;
                    case 3:
                        Console.WriteLine("\nСalculation of the sum of arrays with the same dimensions:\n");
                        //Вычисление суммы массивов с одинаковыми размерностями
                        Console.WriteLine("\nThe sum of the FIRST and SECOND matrix:\n");
                        (first + second).PrintMatrix();
                        Console.WriteLine("\nThe sum of the FIRST and THIRD matrix:\n");
                        (first + third).PrintMatrix();
                        Console.WriteLine("\nThe sum of the THIRD and SECOND matrix:\n");
                        (second + third).PrintMatrix();
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    default:
                        Console.WriteLine("This input is undefined");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}


