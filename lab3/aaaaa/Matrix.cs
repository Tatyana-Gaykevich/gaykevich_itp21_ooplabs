﻿using System;

namespace MatrixLib
{
    public class Matrix
    {

        private int[,] matrix;
        private int matrixRow, matrixColumn;

        //Матрица row x column
        public Matrix(int row, int column)
        {
            matrix = new int[row, column];
            matrixRow = row;
            matrixColumn = column;
        }

        //Перегрузка индексатора, чтобы обратится к 
        //элементу матрицы как к элементу двумерного массива
        public int this[int i, int j]
        {
            get { return matrix[i, j]; }
            set { matrix[i, j] = value; }
        }

        private string nameMatrix;
        public string NameM
        {
            set
            {
                nameMatrix = value;
            }
            get
            {
                return nameMatrix;
            }
        }

        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            Matrix resMass = new Matrix(matrix1.matrixRow, matrix1.matrixColumn);
            for (int i = 0; i < matrix1.matrixRow; i++)
            {
                for (int j = 0; j < matrix2.matrixColumn; j++)
                {
                    resMass[i, j] = matrix1[i, j] + matrix2[i, j];
                }
            }
            return resMass;
        }

        /*public static Matrix operator *(Matrix nameMatrix, int countOfZero)
        {
            countOfZero = 0;
            int m = 1;
            for (int i = 0; i < nameMatrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < 0) m *= matrix[i, j];
                    else if (matrix[i, j] == 0) countOfZero++;
                }
            if (m == 1) return 0;
            return m;
        }*/

        //Вычисление произведения отрицательных элементов массива
        public int Multyply(int[,] matrix, out int countOfZero)
        {
            countOfZero = 0;
            int m = 1;
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < 0) m *= matrix[i, j];
                    else if (matrix[i, j] == 0) countOfZero++;
                }
            if (m == 1) return 0;
            return m;
        }

        //Распечатать матрицу
        public void PrintMatrix()
        {
            for (int i = 0; i < matrixRow; i++)
            {
                for (int j = 0; j < matrixColumn; j++)
                    Console.Write("{0}   ", this[i, j]);
                Console.Write("\n");
            }
        }
    }
}
