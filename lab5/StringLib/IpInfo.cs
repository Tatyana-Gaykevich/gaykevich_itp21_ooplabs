﻿using System;
using System.Collections.Generic;
namespace StringLib
{
    public class IpInfo
    {
        public enum Classes
        {
            B, A, C, D, E
        }
        public string Info { get; }
        public Classes ipClass { get; }

        public IpInfo(string ipAdress)
        {
            int[] octets = Array.ConvertAll(ipAdress.Split('.'), s => int.Parse(s)); 
            if(octets[0] > 255 || octets[0] < 0)
                throw new Exception("This ip is not correct");
            Info = ipAdress;
            if (octets[0] < 126)
                ipClass = Classes.A;
            else if (octets[0] < 192)
                ipClass = Classes.B;
            else if (octets[0] < 224)
                ipClass = Classes.C;
            else if (octets[0] < 240)
                ipClass = Classes.D;
            else if (octets[0] <= 255)
                ipClass = Classes.E;
        }
        public override string ToString()
        {
            return Info;
        }
    }
}

