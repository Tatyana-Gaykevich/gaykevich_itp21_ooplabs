﻿using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
namespace StringLib
{
    public class IpParser
    {
        public string FilePath { get; }
        public IpInfo[] ipInfos { get; }
        public IpParser(string path)
        {
            FilePath = path;
            string[] stringAdresses = IpFromFile(FilePath);
            ipInfos = new IpInfo[stringAdresses.Length];
            int i = 0;
            foreach (string ip in stringAdresses)
            {
                try
                {
                    ipInfos[i] = new IpInfo(ip);
                    i += 1;
                }
                catch
                {
                    Console.WriteLine("Invalid ip");
                }
            }
        }
        private string[] IpFromFile(string path)
        {
            Regex ipRegex = new Regex(@"\d+\.\d+\.\d+\.\d+");
            int count = 0;
            using (StreamReader stream = new StreamReader(path))
            {
                foreach (Match match in ipRegex.Matches(stream.ReadToEnd()))
                {
                    try
                    {
                        IpInfo isValid = new IpInfo(match.Value); //Верификация адресса
                        count += 1;
                    }
                    catch { }
                }
            }
            string[] stringAdresses = new string[count];
            using (StreamReader stream = new StreamReader(path)) // Второе открытие потока для того что бы из файла читалось с начала
            {                                                    // а не там где остановилось после предыдущего чтения
                int i = 0;
                foreach (Match match in ipRegex.Matches(stream.ReadToEnd()))
                {
                    try
                    {
                        IpInfo isValid = new IpInfo(match.Value);
                        stringAdresses[i] = match.Value;
                        i += 1;
                    }
                    catch { }
                }
            }
            return stringAdresses;
        }
        public override string ToString()
        {
            StringBuilder stringToReturn = new StringBuilder();
            foreach (IpInfo ip in ipInfos)
            {
                stringToReturn.Append(ip + $" Class is {ip.ipClass}\n");
            }
            return stringToReturn.ToString();
        }
    }
}
